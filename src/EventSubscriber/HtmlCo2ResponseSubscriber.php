<?php

namespace Drupal\co2consumption\EventSubscriber;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Response subscriber to replace the HtmlResponse with a BigPipeResponse.
 *
 *
 */
class HtmlCo2ResponseSubscriber implements EventSubscriberInterface
{

  /**
   * Constructs a HtmlResponseSubscriber object.
   *
   */
  public function __construct()
  {
  }

  /**
   * Adds markers to the response necessary for the BigPipe render strategy.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The event to process.
   */
  public function onRespondEarly(FilterResponseEvent $event)
  {
    if ($event->isMasterRequest()) {
    $response = $event->getResponse();

    $size = strlen($response->getContent());

    print_r($response->getContent());
//    exit;
    ksm($size);
    ksm(getStatistics($size));

    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespondEarly', 3];

    // Run as the last possible subscriber.
    $events[KernelEvents::RESPONSE][] = ['onRespond', -10000];

    return $events;
  }
}
